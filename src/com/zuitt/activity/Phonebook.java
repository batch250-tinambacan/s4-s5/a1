package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
    // Property
    private ArrayList<Contact> contacts;

    // Constructor
    public Phonebook(){
    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // Getter & Setter
    public ArrayList<Contact> getContact() {
        return this.contacts;
    }

    public void setContact(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Method

    public void getInfo(Contact x) {
        System.out.println("-------------------");
        System.out.println("Name: " + x.getName());
        if (x.getContactNumber() != null){
            System.out.println(x.getName() + " has the following registered number: \n" + x.getContactNumber());
        }
        else {
            System.out.println(x.getName() + " has the following registered number: N/A");
        }

        if (x.getAddress() != null){
            System.out.println(x.getName() + " has the following registered address: \n" + x.getAddress());
        }
        else {
            System.out.println(x.getName() + " has the following address number: N/A");
        }
        System.out.println("-------------------");
    }
}
