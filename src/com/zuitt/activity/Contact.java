package com.zuitt.activity;
import java.util.ArrayList;

public class Contact {
    // Properties
    private String name, contactNumber, address;

    // Default Constructors
    public Contact() {
    }

    // Parameterized Constructors
    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // Overloading Constructors
    public Contact(String name) {
        this.name = name;
        contactNumber = null;
        address = null;
    }

    // Getter & Setter
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return this.contactNumber;
    }
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return this.name + " " + this.contactNumber + " " + this.address;
    }

    // Method
    public void getInfo(){
        System.out.println(name + " has the ff registered number: " + contactNumber);
        System.out.println(name + " has the ff registered address: " + address);
    }



}
