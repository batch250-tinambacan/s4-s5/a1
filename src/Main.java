import com.zuitt.activity.Contact;
import com.zuitt.activity.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        // Instantiated a Phonebook
        Phonebook phonebook = new Phonebook();

        // Instantiated contacts
        Contact contact1 = new Contact("Betty", "0912345666", "Texas");
        Contact contact2 = new Contact("Este");

        // Contain contacts in ArrayList
        ArrayList<Contact> contactList = new ArrayList<Contact>();
        contactList.add(contact1);
        contactList.add(contact2);

        // add to Phonebook
        phonebook.setContact(contactList);

        // call getInfo() if phonebook is not empty
        if (phonebook == null) {
            System.out.println("No contacts found!");
        }
        else {
            for (int i = 0; i < contactList.size(); i++) {
                phonebook.getInfo(contactList.get(i));
            }
        }

//        phonebook.getInfo(contact1);
//        phonebook.getInfo(contact2);

        }
    }




